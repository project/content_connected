<?php

/**
 * @file
 * Hooks provided by the content connected module.
 */

/**
 * Alter reference array or add own module rule.
 *
 * @param object $node
 *   The node object.
 * @param array $references
 *   Reference list.
 *
 * @see content_connected_entityfield()
 *
 * @ingroup content_connected
 */
function hook_content_connected_alter($node, &$references) {

  /* Referencce needs to be added as array with index of nodeid.
   * Example $references[] = array('NODE ID' => t('TYPE OF FIELD'));
   */
}
