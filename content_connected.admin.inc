<?php

/**
 * @file
 * Content administration and module settings UI.
 */

/**
 * Settings page for content connected module.
 */
function content_connected_admin_settings() {
  $form['content_connected_exclude_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields working with content connected'),
  );
  $entity_ref = array();
  $fields = content_connected_get_entityreferencefields();
  foreach ($fields as $field) {
    $entity_ref[$field->field_name] = $field->field_name;
  }
  $form['content_connected_exclude_fieldset']['content_connected_exclude_entityreffields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Excluded entity reference fields'),
    '#options' => $entity_ref,
    '#default_value' => variable_get('content_connected_exclude_entityreffields', ''),
  );

  $textareas = array();
  $fields = content_connected_get_textareafields();
  foreach ($fields as $field) {
    $textareas[$field->field_name] = $field->field_name;
  }

  $form['content_connected_exclude_fieldset']['content_connected_exclude_textareafields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Excluded textarea fields'),
    '#options' => $textareas,
    '#default_value' => variable_get('content_connected_exclude_textareafields', ''),
  );

  return system_settings_form($form);
}
